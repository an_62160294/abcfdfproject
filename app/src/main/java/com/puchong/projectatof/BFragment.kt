package com.puchong.projectatof

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.puchong.projectatof.databinding.FragmentBBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentBBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnGoA?.setOnClickListener {
            val action = BFragmentDirections.actionBFragmentToAFragment()
            view.findNavController().navigate(action)
        }
        binding?.btnGoC?.setOnClickListener {
            val action = BFragmentDirections.actionBFragmentToCFragment()
            view.findNavController().navigate(action)
        }
        binding?.btnGoD?.setOnClickListener {
            val action = BFragmentDirections.actionBFragmentToDFragment()
            view.findNavController().navigate(action)
        }
        binding?.btnGoE?.setOnClickListener {
            val action = BFragmentDirections.actionBFragmentToEFragment()
            view.findNavController().navigate(action)
        }
        binding?.btnGoF?.setOnClickListener {
            val action = BFragmentDirections.actionBFragmentToFFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}